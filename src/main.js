import Vue from "vue";
import App from "./App.vue";
import VueModalTor from "vue-modaltor";
Vue.use(VueModalTor, {
  bgPanel: "#7957d5"
});

new Vue({
  el: "#app",
  render: (h) => h(App)
});
